import unittest
from src.gui import GUI

class TestGUIFunctions(unittest.TestCase):
      
    def setUp(self):
        self.GUI = GUI()

    def test_getCanvas_Width(self):
        self.assertEqual(self.GUI.getCanvas_Width(), 500) #Default value
        self.GUI.setCanvas_Width(400)
        self.assertEqual(self.GUI.getCanvas_Width(), 400) #New value

    def test_getCanvas_Heigth(self):
        self.assertEqual(self.GUI.getCanvas_Heigth(), 300) #Default value
        self.GUI.setCanvas_Heigth(340)
        self.assertEqual(self.GUI.getCanvas_Heigth(), 340) #New value

    def test_getCols_Number(self):
        self.assertEqual(self.GUI.getCols_Number(), 4) #Default value
        self.GUI.setCols_Number(111)
        self.assertEqual(self.GUI.getCols_Number(), 111) #New value

    def test_getRows_Number(self):
        self.assertEqual(self.GUI.getRows_Number(), 4) #Default value
        self.GUI.setRows_Number(220)
        self.assertEqual(self.GUI.getRows_Number(), 220) #New value
