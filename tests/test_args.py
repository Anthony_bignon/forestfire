import unittest
from src.args import ArgHandler

class TestArgHandlerFunctions(unittest.TestCase):
      
    def setUp(self):
        self.arg = ArgHandler()

    def test_handlerRows(self):
        self.assertEqual(self.arg.getRows(), 3) #Default value