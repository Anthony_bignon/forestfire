import argparse

class ArgHandler():
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.handlerRows(self.parser)
        self.handlerCols(self.parser)
        self.handlerForestation(self.parser)
        self.handlerCellSize(self.parser)
    
    def handlerRows(self, parser):
        self.parser.add_argument("-rows", type=int, default=3, help="enter the number of rows")
    
    def handlerCols(self, parser):
        self.parser.add_argument("-cols", type=int, default=4, help="enter the number of cols")

    def handlerForestation(self, parser):
        self.parser.add_argument("-afforestation", type=float, default=0.6, help="0.01<=number<=0.99")

    def handlerCellSize(self, parser):
        self.parser.add_argument("-cell_size", type=int, default=30, help="enter the number of rows")
        
    def parse_args(self):
        args = self.parser.parse_args()
        return args

    def getRows(self):
        args = self.parse_args()
        return args.rows

    def getCols(self):
        args = self.parse_args()
        return args.cols

    def getCellSize(self):
        args = self.parse_args()
        return args.cell_size

    def getForestaton(self):
        args = self.parse_args()
        return args.afforestation
    