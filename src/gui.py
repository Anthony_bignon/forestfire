from tkinter import * 

class GUI():
    def __init__(self):
        self.root = self.createWindow()
        self.setCanvas_Width(500)
        self.setCanvas_Heigth(300)
        self.setCols_Number(4)
        self.setRows_Number(4)
        self.newCanva(self.root)
        self.createGrid(self.root)
    
    def setCanvas_Width(self, canv_width):
        self.canvas_width = canv_width

    def setCanvas_Heigth(self, canv_heigth):
        self.canvas_heigth = canv_heigth
    
    def setRows_Number(self, rowsNb):
        self.rows_number = rowsNb

    def setCols_Number(self, colsNb):
        self.cols_number = colsNb
    
    def getCols_Number(self):
        return self.cols_number 

    def getRows_Number(self):
        return self.rows_number 

    def getCanvas_Width(self):
        return self.canvas_width
        
    def getCanvas_Heigth(self):
        return self.canvas_heigth

    def createWindow(self):
        return Tk()
    
    def startSimulation(self): 
        self.root.mainloop()
    
    def newCanva(self,master):
        self.canvas = Canvas(master,
                width=self.getCanvas_Width(),
                height =self.getCanvas_Heigth())
        self.canvas.pack()

    def createGrid(self,master):    
        self.canvas.create_line(self.getCanvas_Width() / 4, 3, self.getCanvas_Width(), 3, width=5, fill="blue")#top's line
        self.canvas.create_line(self.getCanvas_Width() / 4, self.getCanvas_Heigth()-1, self.getCanvas_Width(), self.getCanvas_Heigth()-1, width=8, fill="blue")#bottom line
        self.canvas.create_line(self.getCanvas_Width() / 4, 0, self.getCanvas_Width() / 4, self.getCanvas_Heigth()-1, width=5, fill="blue") #left line
        self.canvas.create_line(self.getCanvas_Width() -1, 0, self.getCanvas_Width() -1, self.getCanvas_Heigth()-1, width=5, fill="blue")#right line
