import gui
import args

class App():
    def __init__(self):
        self.ArgHandler = args.ArgHandler()
        self.GUI = gui.GUI()

def main():
    app = App()

if __name__ == "__main__":
    main()